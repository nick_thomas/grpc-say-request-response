use hello::say_server::{Say, SayServer};
use hello::{SayRequest, SayResponse};
use tokio::sync::mpsc;
use tonic::{transport::Server, Request, Response, Status};
mod hello;

// defining a strucut for our service
#[derive(Default)]
pub struct MySay {}

// implementing rpc for service definied in .proto
#[tonic::async_trait]
impl Say for MySay {
    /* type SendStreamStream = mpsc::Receiver<Result<SayResponse, Status>>;
    // implementation for rpc call
    async fn send_stream(
        &self,
        request: Request<SayRequest>,
    ) -> Result<Response<Self::SendStreamStream>, Status> {
        // creating a queue or channel
        let (mut tx, rx) = mpsc::channel(4);
        //creating a new task
        tokio::spawn(async move {
            // looping and sending our response using stream
            for _ in 0..4 {
                // sending response to our channel
                tx.send(Ok(SayResponse {
                    message: format!("Hello"),
                }))
                .await;
            }
        });
        // returning our receiver so that tonic can listen on reciever and send the response to client
        Ok(Response::new(rx))
    }
    // our rpc implemented as function
    async fn send(&self, request: Request<SayRequest>) -> Result<Response<SayResponse>, Status> {
        // returning a response as SayResponse message as defined in .proto
        Ok(Response::new(SayResponse {
            //reading data from request which is a wrapper around our SayRequest message defined in .proto
            message: format!("hello {}", request.get_ref().name),
        }))
    }
    // create a new rpc to recieve a stream
    async fn receive_stream(
        &self,
        request: Request<tonic::Streaming<SayRequest>>,
    ) -> Result<Response<SayResponse>, Status> {
        // converting request into stream
        let mut stream = request.into_inner();
        let mut message = String::from("");
        // listening on stream
        while let Some(req) = stream.message().await? {
            message.push_str(&format!("Hello {}\n", req.name))
        }

        // returning response
        Ok(Response::new(SayResponse { message }))
    } */
    type BidirectionalStream = mpsc::Receiver<Result<SayResponse,Status>>;
    async fn bidirectional(
        &self,
        request:Request<tonic::Streaming<SayRequest>>,
    ) -> Result<Response<Self::BidirectionalStream>,Status> {
        // converting request in stream
        let mut streamer = request.into_inner();
        // creating queue
        let (mut tx,rx) = mpsc::channel(4);
        tokio::spawn(async move {
            // listening on request stream
            while let Some(req) = streamer.message().await.unwrap(){
                // sending data as soon as it is available
                tx.send(Ok(SayResponse {
                    message: format!("Hello {}",req.name),
                })).await;
            }
        });
        Ok(Response::new(rx))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // defining address for our service
    let addr = "[::1]:50051".parse().unwrap();
    // creating a service
    let say = MySay::default();
    println!("Server listening on {}", addr);
    // adding our service to our server.
    Server::builder()
        .add_service(SayServer::new(say))
        .serve(addr)
        .await?;
    Ok(())
}
